all: SNVMix2-devel

samtools:
	cd samtools-0.1.6 && $(MAKE)

SNVMix2-devel.o: SNVMix2.c SNVMix2.h
	gcc -O2 -Isamtools-0.1.6/ -o SNVMix2-devel.o -c SNVMix2.c -funroll-loops

SNVMix2-devel: SNVMix2-devel.o samtools $(LOBJS)
	gcc -O2 -Isamtools-0.1.6/ -Lsamtools-0.1.6/ -lbam -lm -lz  SNVMix2-devel.o $(LOBJS) -o SNVMix2-devel
	ln -s SNVMix2-devel SNVMix2


clean:
	rm -f SNVMix2-devel SNVMix2 SNVMix2-devel.o && cd samtools-0.1.6 && $(MAKE) clean


LOBJS= samtools-0.1.6/bgzf.o samtools-0.1.6/kstring.o samtools-0.1.6/bam_aux.o samtools-0.1.6/bam.o samtools-0.1.6/bam_import.o samtools-0.1.6/sam.o samtools-0.1.6/bam_index.o\
	samtools-0.1.6/bam_pileup.o samtools-0.1.6/bam_lpileup.o samtools-0.1.6/bam_md.o samtools-0.1.6/glf.o samtools-0.1.6/razf.o samtools-0.1.6/faidx.o samtools-0.1.6/knetfile.o\
	samtools-0.1.6/bam_sort.o

